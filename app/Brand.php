<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded = [];

    public function models() {
    	return $this->hasMany("App\BrandModel","brand_id","id")->with('items');
    }
}
