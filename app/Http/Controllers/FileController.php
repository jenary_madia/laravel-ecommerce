<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Auth\Access\Response;
use App\Upload;

class FileController extends Controller
{
    public function upload(Request $request) {
        $request_images = $request->file('images');
    	$images = [];
        $validator = Validator::make($request->all(), [
            'images.*' => 'image',
        ]);

        if ($validator->fails()) {
            return Response([
                'success' => false,
                'message' => "Please upload image only.",
            ],200);
        }
        foreach ($request_images as $key) {
            $hashName = pathinfo($key->hashName(), PATHINFO_FILENAME);
            $key->storeAs('public/uploads',$hashName.'.'.$key->extension());
            $upload = Upload::create([
                "filesize" => $key->getClientSize(),
                "original_filename" => $key->getClientOriginalName().'.'.$key->extension(),
                "random_filename" => $hashName.'.'.$key->extension()
            ]);

            array_push($images,[
            	'upload_id' => $upload->id,
            	'path' => asset('storage/uploads/'.$hashName.'.'.$key->extension()),
            ]);

        }

        return Response([
            'success' => true,
            'message' => "Image successfully uploaded",
            'images' =>  $images
        ],200);

    }
}
