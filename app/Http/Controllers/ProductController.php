<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\ItemImages;
use Response;
use App\Category;
use App\Upload;
use App\Brand;
use DB;

class ProductController extends Controller
{
    public function index()
    {
        $categories = Category::where([
            'active' => 1
        ])->pluck('name','id');
        // $items = BrandModel::where('active',1)->get();
        $uploads = Upload::where([
            "brand_id" => null,
            "model_id" => null,
            "category_id" => null
        ])->get();

        $items = Upload::where("brand_id","!=",null)
        ->where("model_id","!=",null)
        ->where("category_id","!=",null)
        ->get();
        $brands = Brand::where("active",1)->pluck("name","id");
        $categories = Category::where("active",1)->pluck("name","id");
    	return view('admin.products',compact('categories','uploads','brands','items'));
    }

    public function paginate($buildup) {
        return Items::paginate(5);
    }

    public function store(Request $request) {
        DB::beginTransaction();
        try {
            $item_id = Item::create([
                'category_id' => $request->category,
                'name' =>  $request->name,
                'price' =>  $request->price,
                'desc' =>  $request->desc,
                'status' => 1
            ])->id;
            $images = [];
            foreach ($request->file('images') as $key) {
                $path = $key->store('public/products');
                array_push($images,[
                    'item_id' => $item_id,
                    'name' => $key->hashName()
                ]);
            }
            ItemImages::insert($images);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function tagImages(Request $request) {
        DB::beginTransaction();
        try {
            $images_ids = $request->to_tag_images;
            Upload::whereIn("id",$images_ids)->update($request->tag_details);
            DB::commit();
            return Response::json([
                'SUCCESS' => true,
                'MESSAGE' =>  "Items successfully updated",
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
        }
        
    }

    public function fetch(Request $request) {
        $items = Upload::where($request->only('brand_id','model_id','category_id'))->get();
        for ($i=0; $i < count($items); $i++) { 
            $items[$i]['random_filename'] = asset("storage/uploads/".$items[$i]['random_filename']);
        }
        return $items;
    }

    public function updateItem(Request $request) {
        DB::beginTransaction();
        try {
            Upload::find($request->item_id)->update($request->item_details); 
            DB::commit();
            return Response::json([
                'SUCCESS' => true,
                'MESSAGE' =>  "Item successfully updated",
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
}
