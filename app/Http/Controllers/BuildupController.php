<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\BrandModel;
use App\Category;
use DB;
use Response;

class BuildupController extends Controller
{
    public function show($buildup) {
    	$brands = Brand::where('active',1)->get();
    	$models = BrandModel::where('active',1)->get();
    	return view('admin.buildup',compact('buildup','brands','models'));
    }

    public function store(Request $request) {
    	DB::beginTransaction();
    	try {
    		switch ($request->type) {
	    		case 'brand':
	    			Brand::create([
	    				'name' => $request->name,
	    				'desc' => $request->desc,
	    				'active' => $request->status,
	    			]);
	    			break;
	    		case 'model':
	    			BrandModel::create([
	    				'name' => $request->name,
	    				'brand_id' => $request->brand,
	    				'desc' => $request->desc,
	    				'active' => $request->status,
	    			]);

	    			break;
    			case 'category':
	    			Category::create([
	    				'name' => $request->name,
	    				'desc' => $request->desc,
	    				'active' => $request->status,
	    			]);

    			break;
	    	}	
	    	DB::commit();
	    	return Response::json([
                'SUCCESS' => true,
                'MESSAGE' =>  "$request->type successfully added",
            ], 200);
    	} catch (Exception $e) {
    		DB::rollback();
    	}
    }

    public function paginate($buildup) {
    	switch ($buildup) {
    		case 'brand':
			    	return Brand::paginate(5);
    			break;
    		
    		case 'model':
			    	return BrandModel::join('brands','brands.id','models.brand_id')->select('models.*','brands.name as brand_name')->paginate(5);
    			break;

			case 'category':
			    	return Category::paginate(5);
    			break;
    	}
    }

    public function delete(Request $request) {

    }

    public function edit(Request $request,$id) {
        DB::beginTransaction();
        try {
            switch ($request->type) {
                case 'brand':
                    Brand::where([
                        'id' => $id
                    ])
                    ->update([
                        'name' => $request->name,
                        'desc' => $request->desc,
                        'active' => $request->active,
                    ]);
                    break;
                case 'model':
                    BrandModel::where([
                        'id' => $id
                    ])
                    ->update([
                        'name' => $request->name,
                        'brand_id' => $request->brand_id,
                        'desc' => $request->desc,
                        'active' => $request->active,
                    ]);

                    break;
                case 'category':
                    Category::where([
                        'id' => $id
                    ])
                    ->update([
                        'name' => $request->name,
                        'desc' => $request->desc,
                        'active' => $request->active,
                    ]);

                break;
            }   
            DB::commit();
            return Response::json([
                'SUCCESS' => true,
                'MESSAGE' =>  "$request->type successfully edited",
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json([
                'SUCCESS' => false,
                'MESSAGE' =>  "$e->getMessage()",
            ], 200);
        } 
    }

    public function fetch(Request $request) {
        switch ($request->action) {
            case "fetch-model":
                return BrandModel::where([
                    "active" => 1,
                    "brand_id" => $request->brand_id,
                ])->pluck("name","id");
            break;
        }
        // return [
            // "brands" => Brand::where("active",1)->pluck("name","id"),
            // "categories" => Category::where("active",1)->pluck("name","id")
        // ];
    }
}
