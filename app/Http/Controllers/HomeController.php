<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Item;
use App\ItemImages;
use App\Category;
use App\Brand;
use App\BrandModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::with('images')->where('status',1)->get();
        $categories = Category::where('active',1)->pluck("name","id");
        $brands = Brand::where('active',1)->with('models')->get();
        return view('shop.index',compact('items','categories','brands'));
    }
}
