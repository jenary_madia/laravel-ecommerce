<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];

    public function images() {
    	return $this->hasMany('App\ItemImages','item_id','id');
    }
}
