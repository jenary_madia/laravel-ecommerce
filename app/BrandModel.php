<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
	protected $table = "models";
    protected $guarded = [];

    public function items() {
    	return $this->hasMany("App\Upload","model_id","id");
    }
}
