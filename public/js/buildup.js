/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 42);
/******/ })
/************************************************************************/
/******/ ({

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(43);


/***/ }),

/***/ 43:
/***/ (function(module, exports) {

// create constructor
var brand_edit = Vue.extend({
    template: '<div>\n                    <div class="modal-body">\n                        <div class="col-md-12">\n                                <div class="form-group col-md-6"><label for="name">Name</label>\n                                    <input type="email" v-model="for_edit.name" class="form-control input-sm">\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label for="price">Description</label>\n                                    <input type="email" v-model="for_edit.desc" class="form-control input-sm">\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label>Status</label>\n                                    <select v-model="for_edit.active" class="form-control input-sm">\n                                        <option value="1">Active</option>\n                                        <option value="0">Inactive</option>\n                                    </select>\n                                </div>\n                        </div>\n                        <div class="clearfix"></div>\n                    </div>\n                    <div class="modal-footer">\n                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>\n                        <button type="button" class="btn btn-primary btn-xs" @click.prevent="save">Save changes</button>\n                    </div>\n                </div>',
    data: function data() {
        return {
            for_edit: {}
        };
    },
    methods: {
        save: function save() {
            buildupp.editData('brand', this.for_edit);
        }
    },
    mounted: function mounted() {
        Vue.set(this, 'for_edit', {
            'name': buildupp.for_edit.name,
            'desc': buildupp.for_edit.desc,
            'active': buildupp.for_edit.active
        });
    },

    watch: {
        // for_edit : {
        //     handler: function (val, oldVal) { 
        //         Vue.set(buildupp,'for_edit',this.for_edit);
        //     },
        //     deep: true
        // }
    }
});

var model_edit = Vue.extend({
    template: '<div>\n                    <div class="modal-body">\n                        <div class="col-md-12">\n                                <div class="form-group col-md-6"><label for="name">Name</label>\n                                    <input type="email" v-model="for_edit.name" class="form-control input-sm">\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label for="price">Description</label>\n                                    <input type="email" v-model="for_edit.desc" class="form-control input-sm">\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label for="model_name">Brand Name</label>\n                                    <select v-model="for_edit.brand_id" class="form-control input-sm">\n                                        <option v-for="brand in brands" :value="brand.id" v-text="brand.name"></option>\n                                    </select>\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label>Status</label>\n                                    <select v-model="for_edit.active" class="form-control input-sm">\n                                        <option value="1">Active</option>\n                                        <option value="0">Inactive</option>\n                                    </select>\n                                </div>\n                        </div>\n                        <div class="clearfix"></div>\n                    </div>\n                    <div class="modal-footer">\n                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>\n                        <button type="button" class="btn btn-primary btn-xs" @click.prevent="save">Save changes</button>\n                    </div>\n                </div>',
    data: function data() {
        return {
            for_edit: {},
            brands: JSON.parse(brands)
        };
    },
    methods: {
        save: function save() {
            buildupp.editData('model', this.for_edit);
        }
    },
    mounted: function mounted() {
        Vue.set(this, 'for_edit', {
            'name': buildupp.for_edit.name,
            'desc': buildupp.for_edit.desc,
            'active': buildupp.for_edit.active,
            'brand_id': buildupp.for_edit.brand_id
        });
    },

    watch: {
        // for_edit : {
        //     handler: function (val, oldVal) { 
        //         Vue.set(buildupp,'for_edit',this.for_edit);
        //     },
        //     deep: true
        // }
    }
});

var category_edit = Vue.extend({
    template: '<div>\n                    <div class="modal-body">\n                        <div class="col-md-12">\n                                <div class="form-group col-md-6"><label for="name">Name</label>\n                                    <input type="email" v-model="for_edit.name" class="form-control input-sm">\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label for="price">Description</label>\n                                    <input type="email" v-model="for_edit.desc" class="form-control input-sm">\n                                </div>\n                                <div class="form-group col-md-6">\n                                    <label>Status</label>\n                                    <select v-model="for_edit.active" class="form-control input-sm">\n                                        <option value="1">Active</option>\n                                        <option value="0">Inactive</option>\n                                    </select>\n                                </div>\n                        </div>\n                        <div class="clearfix"></div>\n                    </div>\n                    <div class="modal-footer">\n                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>\n                        <button type="button" class="btn btn-primary btn-xs" @click.prevent="save">Save changes</button>\n                    </div>\n                </div>',
    data: function data() {
        return {
            for_edit: {},
            brands: JSON.parse(brands)
        };
    },
    methods: {
        save: function save() {
            buildupp.editData('category', this.for_edit);
        }
    },
    mounted: function mounted() {
        Vue.set(this, 'for_edit', {
            'name': buildupp.for_edit.name,
            'desc': buildupp.for_edit.desc,
            'active': buildupp.for_edit.active
        });
    },

    watch: {
        // for_edit : {
        //     handler: function (val, oldVal) { 
        //         Vue.set(buildupp,'for_edit',this.for_edit);
        //     },
        //     deep: true
        // }
    }
});

var buildupp = new Vue({
    el: '#buildup',
    mounted: function mounted() {
        var self = this;
        axios.get(base_url + '/build-up/paginate/' + buildup).then(function (response) {
            self.updateList(buildup, response.data);
        }).catch(function (error) {
            console.log(error);
        });
    },

    data: {
        message: "TGBTG",
        brand: {
            name: '',
            desc: '',
            status: 1
        },
        model: {
            name: '',
            brand: '',
            desc: '',
            status: 1
        },
        category: {
            name: '',
            brand: '',
            model: '',
            desc: '',
            status: 1
        },
        list_brand: {},
        list_model: {},
        list_category: {},
        for_edit: {}
    },
    methods: {
        reset: function reset() {
            var self = this;
            axios.get(base_url + '/build-up/paginate/' + buildup).then(function (response) {
                self.updateList(buildup, response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        changePage: function changePage(type, page) {
            var self = this;
            switch (type) {
                case 'brand':
                    var ajax = axios.get(this.list_brand[page]);
                    break;

                case 'model':
                    var ajax = axios.get(this.list_model[page]);
                    break;

                case 'category':
                    var ajax = axios.get(this.list_category[page]);
                    break;
            }

            ajax.then(function (response) {
                self.updateList(buildup, response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        addData: function addData(type) {
            // swal("Hello world!");
            var self = this;
            var parameters = void 0;
            switch (type) {
                case 'brand':
                    parameters = this.brand;
                    break;

                case 'model':
                    parameters = this.model;
                    break;

                case 'category':
                    parameters = this.category;
                    break;
            }
            parameters.type = type;
            axios.post(base_url + '/build-up/store', parameters).then(function (response) {
                if (response.data.SUCCESS == true) {
                    swal("Good job!", response.data.MESSAGE, "success");
                } else {
                    swal("Ooops!", response.data.MESSAGE, "error");
                }
                self.clearInputs();
            }).catch(function (error) {
                swal("Good job!", "Something went wrong", "error");
            });
            this.reset();
        },
        editData: function editData(type, data) {
            console.log(data);
            var self = this;
            var parameters = data;
            parameters.type = type;
            axios.post(base_url + '/build-up/edit/' + this.for_edit.id, parameters).then(function (response) {
                if (response.data.SUCCESS == true) {
                    swal("Good job!", response.data.MESSAGE, "success");
                    Vue.set(self.for_edit, 'name', data.name);
                    Vue.set(self.for_edit, 'desc', data.desc);
                    Vue.set(self.for_edit, 'active', data.active);
                    Vue.set(self.for_edit, 'brand_id', data.brand_id);
                } else {
                    swal("Ooops!", response.data.MESSAGE, "error");
                }
            }).catch(function (error) {
                swal("", error.message, "error");
            });
            // this.reset();
            $('#edit-buidup').modal('hide');
        },
        getModels: function getModels() {
            console.log(this.category.brand);
        },
        updateList: function updateList(buildup, data) {
            switch (buildup) {
                case 'brand':

                    Vue.set(this, 'list_brand', data);

                    break;

                case 'model':

                    Vue.set(this, 'list_model', data);

                    break;

                case 'category':

                    Vue.set(this, 'list_category', data);

                    break;
            }
        },
        clearInputs: function clearInputs() {
            for (var i = 0; i < Object.keys(this[buildup]).length; i++) {
                var key = Object.keys(this[buildup])[i];
                Vue.set(this[buildup], key, '');
            }
        },
        forEdit: function forEdit(type, index) {
            Vue.set(this, "for_edit", this[type]['data'][index]);
            var component = void 0;
            switch (type) {
                case 'list_brand':
                    component = new brand_edit().$mount();
                    break;
                case 'list_model':
                    component = new model_edit().$mount();
                    break;
                case 'list_category':
                    component = new category_edit().$mount();
                    break;
            }
            document.getElementById('edit-form').innerHTML = "";
            document.getElementById('edit-form').appendChild(component.$el);
            $('#edit-buidup').modal('show');
        },
        clickThis: function clickThis() {
            alert('hey');
        }
    }
});

/***/ })

/******/ });