import RowProducts from './components/products/index';

const app = new Vue({
    el: '#products',
    mounted() {
    },
    components: {
        RowProducts,
    },
    data : {
	    create : {
            name : "",
            price : 0,
            category : "",
            desc : "",
            images : []
        },
        tag_details : {
            brand_id : 0,
            model_id : 0,
            category_id : 0,
        },
        to_tag_images : [],
        list_model : [],
        recently_uploaded : []
    },
    methods : {
    	onFileChange(e) {
            let data = new FormData();
            let file_size = 0;
            let files = e.target.files || e.dataTransfer.files;
            for (var i = 0; i < files.length; i++) {
                file_size += files.item(i).size;
                data.append('images[' + i + ']', files.item(i), files.item(i).name);
            }
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }
            var self = this;
            axios.post(base_url+'/file-upload', data, config)
            .then(function (response) {
                if(response.data.success) {
                    for (var i = 0; i < response.data.images.length; i++) {
                        var image = response.data.images[i];
                        self.recently_uploaded.push({
                            id : image.upload_id,
                            path : image.path,
                        });
                    }
                    alert(response.data.message);
                }
            })
            .catch(function (error) {
            });
        },
        add() {
            let data = new FormData();
            data.append('name',this.create.name);
            data.append('category',this.create.category);
            data.append('desc',this.create.desc);
            data.append('price',this.create.price);
            for (var i = 0; i < this.create.images.length; i++) {
                data.append('images[' + i + ']', this.create.images[i], this.create.images[i].name);
            }
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }
            return axios.post(base_url+'/products/store', data, config);
        },
        tag_images() {
            var self = this;
            axios.post(base_url+'/products/tag-images',{
                tag_details : this.tag_details,
                to_tag_images : this.to_tag_images
            })
            .then(function (response) {
                alert(response.data.MESSAGE);
                // self.$set(self,'list_model',response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        },
        
    },
    watch : {
        "tag_details.brand_id" : function (val, oldVal) {
            var self = this;
            axios.post(base_url+'/build-up/fetch',{
                brand_id : val,
                action : "fetch-model"
            })
            .then(function (response) {
                self.$set(self,'list_model',response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        },
    }
});

