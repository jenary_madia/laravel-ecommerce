// create constructor
const brand_edit = Vue.extend({
    template: `<div>
                    <div class="modal-body">
                        <div class="col-md-12">
                                <div class="form-group col-md-6"><label for="name">Name</label>
                                    <input type="email" v-model="for_edit.name" class="form-control input-sm">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="price">Description</label>
                                    <input type="email" v-model="for_edit.desc" class="form-control input-sm">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                    <select v-model="for_edit.active" class="form-control input-sm">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-xs" @click.prevent="save">Save changes</button>
                    </div>
                </div>`,
    data: function () {
        return {
            for_edit: {},
        }
    },
    methods : {
        save() {
            buildupp.editData('brand',this.for_edit);
        }
        
    },
    mounted() {
        Vue.set(this,'for_edit',{
            'name' : buildupp.for_edit.name,
            'desc' : buildupp.for_edit.desc,
            'active' : buildupp.for_edit.active,
        });
    },
    watch : {
        // for_edit : {
        //     handler: function (val, oldVal) { 
        //         Vue.set(buildupp,'for_edit',this.for_edit);
        //     },
        //     deep: true
        // }
    }
})


const model_edit = Vue.extend({
    template: `<div>
                    <div class="modal-body">
                        <div class="col-md-12">
                                <div class="form-group col-md-6"><label for="name">Name</label>
                                    <input type="email" v-model="for_edit.name" class="form-control input-sm">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="price">Description</label>
                                    <input type="email" v-model="for_edit.desc" class="form-control input-sm">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="model_name">Brand Name</label>
                                    <select v-model="for_edit.brand_id" class="form-control input-sm">
                                        <option v-for="brand in brands" :value="brand.id" v-text="brand.name"></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                    <select v-model="for_edit.active" class="form-control input-sm">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-xs" @click.prevent="save">Save changes</button>
                    </div>
                </div>`,
    data: function () {
        return {
            for_edit: {},
            brands : JSON.parse(brands)
        }
    },
    methods : {
        save() {
            buildupp.editData('model',this.for_edit);
        }
        
    },
    mounted() {
        Vue.set(this,'for_edit',{
            'name' : buildupp.for_edit.name,
            'desc' : buildupp.for_edit.desc,
            'active' : buildupp.for_edit.active,
            'brand_id' : buildupp.for_edit.brand_id,
        });
    },
    watch : {
        // for_edit : {
        //     handler: function (val, oldVal) { 
        //         Vue.set(buildupp,'for_edit',this.for_edit);
        //     },
        //     deep: true
        // }
    }
})

const category_edit = Vue.extend({
    template: `<div>
                    <div class="modal-body">
                        <div class="col-md-12">
                                <div class="form-group col-md-6"><label for="name">Name</label>
                                    <input type="email" v-model="for_edit.name" class="form-control input-sm">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="price">Description</label>
                                    <input type="email" v-model="for_edit.desc" class="form-control input-sm">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                    <select v-model="for_edit.active" class="form-control input-sm">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-xs" @click.prevent="save">Save changes</button>
                    </div>
                </div>`,
    data: function () {
        return {
            for_edit: {},
            brands : JSON.parse(brands)
        }
    },
    methods : {
        save() {
            buildupp.editData('category',this.for_edit);
        }
        
    },
    mounted() {
        Vue.set(this,'for_edit',{
            'name' : buildupp.for_edit.name,
            'desc' : buildupp.for_edit.desc,
            'active' : buildupp.for_edit.active,
        });
    },
    watch : {
        // for_edit : {
        //     handler: function (val, oldVal) { 
        //         Vue.set(buildupp,'for_edit',this.for_edit);
        //     },
        //     deep: true
        // }
    }
})


const buildupp = new Vue({
    el: '#buildup',
    mounted() {
    	let self = this;
		axios.get(base_url+'/build-up/paginate/'+buildup)
		.then(function (response) {
			self.updateList(buildup,response.data);
		})
		.catch(function (error) {
			console.log(error);
		});
    },
    data : {
        message : "TGBTG",
    	brand : {
    		name : '',
    		desc : '',
    		status : 1,
    	},
    	model : {
    		name : '',
    		brand : '',
    		desc : '',
    		status : 1,
    	},
    	category : {
    		name : '',
    		brand : '',
    		model : '',
    		desc : '',
    		status : 1,
    	},
    	list_brand : {},
    	list_model : {},
    	list_category : {},
        for_edit : {

        }
    },
    methods : {
    	reset() {
    		var self = this;
    		axios.get(base_url+'/build-up/paginate/'+buildup)
			.then(function (response) {
				self.updateList(buildup,response.data);
			})
			.catch(function (error) {
				console.log(error);
			});
    	},
    	changePage(type,page) {
    		var self = this;
			switch(type) {
    			case 'brand' :
					var ajax = axios.get(this.list_brand[page])
    			break;

    			case 'model' :
    				var ajax = axios.get(this.list_model[page])
    			break;

    			case 'category' :
    				var ajax = axios.get(this.list_category[page])
    			break;
    		}

			ajax.then(function (response) {
				self.updateList(buildup,response.data);
			})
			.catch(function (error) {
				console.log(error);
			});
    	},
    	addData(type) {
			// swal("Hello world!");
			var self = this;
    		let parameters;
    		switch(type) {
    			case 'brand' :
    				parameters = this.brand;
    			break;

    			case 'model' :
    				parameters = this.model;
    			break;

    			case 'category' :
    				parameters = this.category;
    			break;
    		}
    		parameters.type = type;
    		axios.post(base_url + '/build-up/store',parameters)
			.then(function (response) {
				if(response.data.SUCCESS == true) {
					swal("Good job!", response.data.MESSAGE, "success");
				}else{
					swal("Ooops!", response.data.MESSAGE, "error");
				}
				self.clearInputs();
			})
			.catch(function (error) {
				swal("Good job!", "Something went wrong", "error");
			});
			this.reset();
    	},

        editData(type,data) {
            console.log(data);
            var self = this;
            let parameters = data;
            parameters.type = type;
            axios.post(base_url + '/build-up/edit/'+this.for_edit.id,parameters)
            .then(function (response) {
                if(response.data.SUCCESS == true) {
                    swal("Good job!", response.data.MESSAGE, "success");
                    Vue.set(self.for_edit,'name',data.name);
                    Vue.set(self.for_edit,'desc',data.desc);
                    Vue.set(self.for_edit,'active',data.active);
                    Vue.set(self.for_edit,'brand_id',data.brand_id);
                }else{
                    swal("Ooops!", response.data.MESSAGE, "error");
                }
            })
            .catch(function (error) {
                swal("", error.message, "error");
            });
            // this.reset();
            $('#edit-buidup').modal('hide');
        },

    	getModels() {
    		console.log(this.category.brand);
    	},

    	updateList(buildup,data) {
    		switch(buildup) {
    			case 'brand' :

    				Vue.set(this,'list_brand',data);

    			break;

    			case 'model' :

    				Vue.set(this,'list_model',data);

    			break;

    			case 'category' :

    				Vue.set(this,'list_category',data);

    			break;
    		}
    	},

    	clearInputs() {
			for (var i = 0; i < Object.keys(this[buildup]).length; i++) {
				var key = Object.keys(this[buildup])[i];
				Vue.set(this[buildup],key,'');
			}
    	},

        forEdit(type,index) {
            Vue.set(this,"for_edit",this[type]['data'][index]);
            let component;
            switch(type) {
                case 'list_brand':
                    component = new brand_edit().$mount();
                    break;
                case 'list_model':
                    component = new model_edit().$mount();
                    break;
                case 'list_category':
                    component = new category_edit().$mount();
                    break;
            }
            document.getElementById('edit-form').innerHTML = "";
            document.getElementById('edit-form').appendChild(component.$el);
            $('#edit-buidup').modal('show');
        },
        clickThis() {
            alert('hey');
        }
    },
});

