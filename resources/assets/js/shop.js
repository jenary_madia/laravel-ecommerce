const shop = new Vue({
    el: '#shop',
    mounted() {
    	// details : {
     //        brand_id : "",
     //        ra
     //    }

    },
    data : {
        tab : 'home',
        model : '',
        items : []
    },
    methods : {
        changeTab(tab,model = null) {
            this.$set(this,'tab',tab);
            this.$set(this,'model',model);
            this.$set(this,'items',[]);
        },
        fetchItems(category) {
            var self = this;
            axios.post(base_url+'/products/fetch',{
                brand_id : this.tab,
                model_id : this.model,
                category_id : category
            })
            .then(function (response) {
                self.$set(self,'items',response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    },
});

