@extends('layouts.app')

@section('content')
<div class="container" id="buildup">
    <div class="row">
        <div class="col-md-12">
            @if($buildup == "brand")
                <div class="panel panel-default">
                    <div class="panel-heading">Brand</div>
                    <div class="panel-body">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="brand_name">Name</label>
                                <input v-model="brand.name" type="text" class="input-sm form-control" id="brand_name">
                            </div>
                            <div class="form-group">
                                <label for="brand_desc">Description</label>
                                <input type="text" v-model="brand.desc" class="input-sm form-control" id="brand_desc">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select v-model="brand.status" class="form-control input-sm">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-xs btn-primary" @click.prevent="addData('brand')">ADD</button>
                        </form>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="data,index in list_brand.data">
                                    <td>@{{ data.name }}</td>
                                    <td>@{{ data.desc }}</td>
                                    <td v-if="data.active == 1">Active</td>
                                    <td v-else="data.active == 0">Inactive</td>
                                    <td>
                                        <button class="btn btn-xs btn-danger">DELETE</button>
                                        <button @click.prevent="forEdit('list_brand',index)" class="btn btn-xs btn-primary">EDIT</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button :disabled="! list_brand.prev_page_url" @click.prevent="changePage('brand','prev_page_url')" type="button">PREV</button>
                        <button :disabled="! list_brand.next_page_url" @click.prevent="changePage('brand','next_page_url')" type="button">NEXT</button>
                    </div>
                </div>
            @elseif($buildup == "model")
                <div class="panel panel-default">
                    <div class="panel-heading">Model</div>
                    <div class="panel-body">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="brand_name">Name</label>
                                <input v-model="model.name" type="text" class="input-sm form-control" id="model_name">
                            </div>
                            <div class="form-group">
                                <label for="model_name">Brand Name</label>
                                <select v-model="model.brand" class="form-control input-sm">
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="model_desc">Description</label>
                                <input type="text" v-model="model.desc" class="input-sm form-control" id="model_desc">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select v-model="model.status" class="form-control input-sm">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-xs btn-primary" @click.prevent="addData('model')">ADD</button>
                        </form>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Brand</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="data,index in list_model.data">
                                    <td>@{{ data.brand_name }}</td>
                                    <td>@{{ data.name }}</td>
                                    <td>@{{ data.desc }}</td>
                                    <td v-if="data.active == 1">Active</td>
                                    <td v-else>Inactive</td>
                                    <td>
                                        <button class="btn btn-xs btn-danger">DELETE</button>
                                        <button @click.prevent="forEdit('list_model',index)" class="btn btn-xs btn-primary">EDIT</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button :disabled="! list_model.prev_page_url" @click.prevent="changePage('model','prev_page_url')" type="button">PREV</button>
                        <button :disabled="! list_model.next_page_url" @click.prevent="changePage('model','next_page_url')" type="button">NEXT</button>
                    </div>
                </div>
            @elseif($buildup == "category")
                <div class="panel panel-default">
                    <div class="panel-heading">Category</div>
                    <div class="panel-body">
                        <form class="form-inline">
                            <div class="form-group">
                                <label>Name</label>
                                <input v-model="category.name" type="text" class="input-sm form-control" id="category_name">
                            </div>
                            <div class="form-group">
                                <label for="model_desc">Description</label>
                                <input type="text" v-model="category.desc" class="input-sm form-control" id="model_desc">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select v-model="model.status" class="form-control input-sm">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-xs btn-primary" @click.prevent="addData('category')">ADD</button>
                        </form>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="data,index in list_category.data">
                                    <td>@{{ data.name }}</td>
                                    <td v-if="data.active == 1">Active</td>
                                    <td v-else>Inactive</td>
                                    <td>
                                        <button class="btn btn-xs btn-danger">DELETE</button>
                                        <button @click.prevent="forEdit('list_category',index)" class="btn btn-xs btn-primary">EDIT</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button :disabled="! list_category.prev_page_url" @click.prevent="changePage('category','prev_page_url')" type="button">PREV</button>
                        <button :disabled="! list_category.next_page_url" @click.prevent="changePage('model','next_page_url')" type="button">NEXT</button>
                    </div>
                </div>
            @endif

            @include('modals.edit-buildup')
        </div>
    </div>
</div>
@endsection

@section('scripts_page')
    <script>
        window.buildup = '{{ $buildup }}';
        window.brands = '{!!  $brands !!}';
    </script>
    <script src="{{ asset('js/buildup.js') }}"></script>
@endsection
