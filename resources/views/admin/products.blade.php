@extends('layouts.app')

@section('content')
<div class="container" id="products">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Products</div>
                <div class="panel-body">
                    <button type="button" class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target="#myModal">ADD</button>
                    
                    <br>
                    <row-products
                        :items = '{{ $items }}'
                    >
                    </row-products> 
            </div>
        </div>
    </div>
    @include('admin.add-product')
</div>
@endsection

@section('scripts_page')
    <script src="{{ asset('js/products.js') }}"></script>
@endsection
