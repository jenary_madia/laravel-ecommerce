<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document" style="width : 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Product</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="file btn btn-xs btn-primary">
                        Upload Images
                        <div class="upload-button">
                            <input id="image-selector" multiple type="file" name="file" @change="onFileChange($event)">
                        </div>
                    </div>
                    <div id="images" class="images row">
                        @foreach($uploads as $image)
                            <div class="col-md-3">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <input type="checkbox" v-model="to_tag_images" value="{{ $image->id }}">
                                    </div>
                                    <div class="panel-body">
                                        <img class='product-thumbnail' src='{{ asset("storage/uploads/$image->random_filename") }}'>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div v-for="item in recently_uploaded" class="col-md-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <input type="checkbox" v-model="to_tag_images" :value="item.id">
                                </div>
                                <div class="panel-body">
                                    <img class='product-thumbnail' :src="item.path">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div> 
                <hr>
                <form class="form-inline">
                    <div class="form-group">
                        <label for="exampleInputName2">Brand : </label>
                        <select style="width: 200px;" name="" id="" class="form-control input-sm" v-model="tag_details.brand_id">
                            @foreach($brands as $value => $key)
                                <option value="{{ $value }}">{{ $key }}</option>
                            @endforeach
                            <option selected disabled value="0"></option>
                        </select>
                        <label for="exampleInputName2">Model : </label>
                        <select style="width: 200px;" name="" id="select-model" class="form-control input-sm" v-model="tag_details.model_id">
                            <option v-for="text,value in list_model" :value="value" v-text="text"></option>
                            <option selected disabled value="0"></option>
                        </select>
                        <label for="exampleInputName2">Category : </label>
                        <select style="width: 200px;" name="" id="select-model" class="form-control input-sm" v-model="tag_details.category_id">
                            @foreach($categories as $value => $key)
                                <option value="{{ $value }}">{{ $key }}</option>
                            @endforeach
                            <option selected disabled value="0"></option>
                        </select>
                    </div>
                    <!-- <div class="form-group">
                        <label for="exampleInputEmail2">Model : </label>
                        <select name="" id="" class="form-control input-sm"></select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail2">Category : </label>
                        <select name="" id="" class="form-control input-sm"></select>
                    </div> -->
                </form>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-xs" @click.prevent="tag_images">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->