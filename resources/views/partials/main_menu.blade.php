<div id="menu_tab">
    <ul class="menu">
        <li><a href="#" class="nav"> Home </a></li>
        <li class="divider"></li>
        <li><a href="#" class="nav">Products</a></li>
        <li class="divider"></li>
        @foreach($brands as $brand)
        <li><a href="#" class="nav">{{ $brand->name }}</a></li>
        <li class="divider"></li>
        @endforeach
        <li><a href="#" class="nav">Shipping </a></li>
        <li class="divider"></li>
        <li><a href="contact.html" class="nav">Contact Us</a></li>
        <li class="divider"></li>
        <li><a href="details.html" class="nav">Details</a></li>
    </ul>
</div>