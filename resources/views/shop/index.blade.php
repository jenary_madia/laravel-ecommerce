 @extends('layouts.shop')

@section('content')
 <div class="center_content">
      <div class="oferta"> <img src="images/p1.png" width="165" height="113" border="0" class="oferta_img" alt="" />
        <div class="oferta_details">
          <div class="oferta_title">Power Tools BST18XN Cordless</div>
          <div class="oferta_text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </div>
          <a href="#" class="prod_buy">details</a> </div>
      </div>
      <div class="center_title_bar">Latest Products</div>
      @foreach($items as $item)
      <div class="prod_box">
        <div class="center_prod_box">
          <div class="product_title"><a href="#">{{ $item->name }}</a></div>
          <div class="product_img"><a href="#"><img width="100" height="100" src="{{ url('storage',$item->images[0]['name']) }}" alt="" border="0" /></a></div>
          <div class="prod_price"><span class="reduce">350$</span> <span class="price">{{ $item->price }}</span></div>
        </div>
        <div class="prod_details_tab"> <a href="#" class="prod_buy">Add to Cart</a> <a href="#" class="prod_details">Details</a> </div>
      </div>
      @endforeach
</div>
@endsection