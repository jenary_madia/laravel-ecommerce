<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{!! url('/') !!}" id="base_url">
    <link rel="icon" href="res/favicon.ico">

    <title>Car Parts Store Page</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front-style.css') }}" rel="stylesheet">
  </head>

  <body>

    <div class="container">
    <div class="page-header">
        <img src="{{ asset('images/logo.jpg') }}" alt="Joe's Bikes" width="auto" height="120px" style="float: left; vertical-align: middle">
    <div class="search-container">
      <form action="/action_page.php">
        <input type="text" placeholder="Search.." name="search">
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
      </div>
    </div>
    <div class="row" id="shop">
    
        <div class="col-md-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active" @click="changeTab('home')"><a href="#tab1primary">Home</a></li>
                          @foreach($brands as $brand)
                            @if(count($brand->models) > 0)
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $brand->name }}
                                @if(count($brand->models) > 0)
                                  <span class="caret"></span>
                                @endif
                              </a>
                              @if(count($brand->models) > 0)
                                <ul class="dropdown-menu">
                                  @foreach($brand->models as $model)
                                      <li><a @click="changeTab({{ $brand->id }},{{ $model->id }})" >{{ $model->name }}</a></li>
                                  @endforeach            
                                </ul> 
                              @endif
                                       
                            </li>
                          @else
                            <li @click="changeTab({{ $brand->id }})">
                              <a>{{ $brand->name }}</a>
                            </li>
                          @endif
                        @endforeach   
                        </ul>
                </div>
                <div class="main-panel-body panel-body">
                    <div v-if="tab == 'home'">
                      Welcome. Please select your manufacturer.
                    </div>
                    <template v-else>
                      <div class="col-md-3">
                        Categories > {{ $brand->name }}
                        <div class="panel-group" id="accordion">
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                  STOCK PRODUCTS
                                </a>
                              </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <ul class="list-unstyled">
                                  @foreach($categories as $value => $text)
                                    <li><a href="#subtab1-1" data-toggle="tab" @click="fetchItems({{ $value }})">{{ $text }}</a></li>
                                  @endforeach
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                  AFTER MARKET
                                </a>
                              </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div v-for="item in items" class="col-md-3">
                          <div class="thumbnail">
                            <a :href="item.random_filename"  >
                              <img :src="item.random_filename" alt="Lights" style="width:100%">
                              <div class="caption">
                                <p>ITEM 1</p>
                                <p>PHP 10,000</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </template>
                </div>
            </div>
        </div>
  </div>
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/shop.js') }}"></script>
  </body>
</html>