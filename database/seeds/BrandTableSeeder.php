<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert(
        	[
	        	[
	        		"name" => "Honda",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	],
	        	[
	        		"name" => "Suzuki",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	],
	        	[
	        		"name" => "Yamaha",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	],
	        	[
	        		"name" => "Kawasaki",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	],
	        	[
	        		"name" => "Kymco",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	],
	        	[
	        		"name" => "Rusi",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	],
	        	[
	        		"name" => "MCX",
	        		"active" => 1,
	        		"created_at" => Carbon::now(),
	        		"updated_at" => Carbon::now(),
	        	]
        	]
    	);
    }
}
