<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
        	[
	        	[
					"name" => "Lights and Bulbs",
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Flairings and Covers",
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Tires and Mags",
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Bolts and Nuts",
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Engines and Accessories",
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Cables and Wires",
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				]
			]
        );
    }
}
