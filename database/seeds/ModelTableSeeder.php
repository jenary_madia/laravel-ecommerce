<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('models')->insert(
	    	[	
		    	[
					"name" => "CBR150",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "RS150",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "RS125",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Click",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Beat",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "TMX",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Wave",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "XRM",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Zoomer X",
					"brand_id" => 1,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Gixxer",
					"brand_id" => 2,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Raider",
					"brand_id" => 2,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Skydrive",
					"brand_id" => 2,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],
				[
					"name" => "Smash",
					"brand_id" => 2,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],

				[
					"name" => "Sniper",
					"brand_id" => 3,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],

				[
					"name" => "FZ",
					"brand_id" => 3,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],

				[
					"name" => "Mio Soul / Sporty / Aerox",
					"brand_id" => 3,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],

				[
					"name" => "NMAX",
					"brand_id" => 3,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],

				[
					"name" => "BJAJ",
					"brand_id" => 4,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				],

				[
					"name" => "FURY",
					"brand_id" => 4,
					"active" => 1,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
				]
			]
        );
    }
}
