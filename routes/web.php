<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('shop.shop');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
	// Route::get('/home', 'HomeController@index')->name('home');
	Route::get('buildup/{buildup}', 'BuildupController@show')->name('buildup');
	Route::prefix('build-up')->group(function () {
		Route::post('store', 'BuildupController@store');
		Route::post('edit/{buildup}', 'BuildupController@edit');
		Route::get('paginate/{buildup}', 'BuildupController@paginate');
		Route::post('fetch', 'BuildupController@fetch');
	});

	Route::prefix('products')->group(function () {
		Route::get('/', 'ProductController@index')->name('products');
		Route::post('store', 'ProductController@store');
		Route::get('paginate', 'ProductController@paginate');
		Route::post('tag-images', 'ProductController@tagImages');
		Route::post('update-item', 'ProductController@updateItem');
		// Route::get('paginate/{buildup}', 'BuildupController@paginate');
	});
});

Route::post('products/fetch', 'ProductController@fetch');

Route::get('storage/{filename}', function ($filename)
{
    // Add folder path here instead of storing in the database.
    $path = storage_path('app/public/products/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::post('/file-upload', 'FileController@upload');

